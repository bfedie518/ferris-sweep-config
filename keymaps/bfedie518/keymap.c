#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT_split_3x5_2(
            KC_Q, KC_W, KC_F, KC_P, KC_B,
            KC_J, KC_L, KC_U, KC_Y, KC_QUOT,

            LCTL_T(KC_A), LALT_T(KC_R), LWIN_T(KC_S), LSFT_T(KC_T), MEH_T(KC_G),
            MEH_T(KC_M), RSFT_T(KC_N), RWIN_T(KC_E), RALT_T(KC_I), RCTL_T(KC_O),

            KC_Z, KC_X, KC_C, KC_D, KC_V,
            KC_K, KC_H, KC_COMM, KC_DOT, KC_SCLN,

            MO(1), LT(4, KC_BSPC),
            LT(5, KC_SPC), MO(2)
            ),

    [1] = LAYOUT_split_3x5_2(
            KC_MPRV, KC_VOLD, KC_VOLU, KC_MNXT, TO(3),
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT, KC_MPLY,
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            KC_HOME, KC_PGDN, KC_PGUP, KC_END, XXXXXXX,
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            XXXXXXX, KC_TRNS,
            KC_TRNS, KC_TRNS
            ),

    [2] = LAYOUT_split_3x5_2(
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            TO(3), KC_7, KC_8, KC_9, KC_PSLS,

            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            KC_PPLS, KC_4, KC_5, KC_6, KC_DOT,

            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            XXXXXXX, KC_1, KC_2, KC_3, KC_0,

            KC_TRNS, KC_TRNS,
            KC_TRNS, XXXXXXX
            ),

    [3] = LAYOUT_split_3x5_2(
            XXXXXXX, KC_BTN3, KC_BTN2, KC_BTN1, QK_BOOT,
            KC_BTN3, KC_F7, KC_F8, KC_F9, KC_F10,

            KC_MS_L, KC_MS_D, KC_MS_U, KC_MS_R, XXXXXXX,
            KC_BTN1, KC_F4, KC_F5, KC_F6, KC_F11,

            KC_WH_L, KC_WH_D, KC_WH_U, KC_WH_R, XXXXXXX,
            KC_BTN2, KC_F1, KC_F2, KC_F3, KC_F12,

            TO(0), KC_TRNS,
            KC_TRNS, TO(0)
            ),

    [4] = LAYOUT_split_3x5_2(
            KC_EXLM, KC_AT, KC_HASH, KC_DLR, KC_GRAVE,
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            LCTL_T(KC_PIPE), LALT_T(KC_ESC), LWIN_T(KC_TAB), LSFT_T(KC_ENT), MEH_T(KC_PLUS),
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            KC_LABK, KC_LCBR, KC_LBRC, KC_LPRN, KC_SLASH,
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,

            KC_TRNS, XXXXXXX,
            KC_TRNS, KC_TRNS
            ),

    [5] = LAYOUT_split_3x5_2(
            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            KC_TILDE, KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR,

            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            MEH_T(KC_MINS), RSFT_T(KC_ENT), RWIN_T(KC_TAB), RALT_T(KC_ESC), RCTL_T(KC_EQUAL),

            KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            KC_BSLS, KC_RPRN, KC_RBRC, KC_RCBR, KC_RABK,

            KC_TRNS, KC_TRNS,
            XXXXXXX, KC_TRNS
            ),

};
const uint16_t PROGMEM caps_lock_combo[] = {LSFT_T(KC_T), RSFT_T(KC_N), COMBO_END};
combo_t key_combos[] = {
    COMBO(caps_lock_combo, KC_CAPS_LOCK),
};

const key_override_t underscore_override = ko_make_basic(MOD_MASK_SHIFT, KC_COMM, KC_UNDS);
const key_override_t question_override = ko_make_basic(MOD_MASK_SHIFT, KC_DOT, KC_QUES);
const key_override_t delete_override = ko_make_basic(MOD_MASK_SHIFT, LT(4, KC_BSPC), KC_DEL);
const key_override_t alt_f4_override = ko_make_basic(MOD_MASK_CSA, KC_4, LALT(KC_F4));
const key_override_t snip_override = ko_make_basic(MOD_MASK_CSA, KC_P, LSG(KC_S));

// This globally defines all key overrides to be used
const key_override_t **key_overrides = (const key_override_t *[]){
    &underscore_override,
        &question_override,
        &delete_override,
        &alt_f4_override,
        &snip_override,
        NULL // Null terminate the array of overrides!
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case LCTL_T(KC_PIPE):
            if (record->tap.count && record->event.pressed) {
                tap_code16(KC_PIPE);
                return false;        // Return false to ignore further processing of key
            }
            break;
    }
    return true;
}


#if defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {

};
#endif // defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)





